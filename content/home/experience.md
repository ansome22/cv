+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Web Developer"
  company = "Alpha Careers"
  company_url = ""
  location = "Hamilton, New Zealand"
  date_start = "2018-07-01"
  date_end = "2018-07-31"
  description = """Responsibilities include:
  * Automation tasks
  * User account system
  * Email System
  * PHP Development """

[[experience]]
  title = "Multidisciplinary Design thinker"
  company = "Design Factory"
  company_url = ""
  location = "Hamilton, New Zealand"
  date_start = "2017-07-01"
  date_end = "2017-11-30"
  description = """Responsibilities: 
  *  Interviewing
  *  Design Thinking
  * Ideating Solutions
  *  Prototyping Solutions
  *  Project Planning
  *  Website Development
  *  Node Development """

[[experience]]
  title = "Business Support Administration"
  company = "Fire and Emergency New Zealand"
  company_url = ""
  location = "Hamilton, New Zealand"
  date_start = "2016-03-01"
  date_end = "2017-07-30"
  description = """Responsibilities: 
* Database entry
* Confer with co-workers to coordinate work activities.    
* File documents or records. 
* Photocopying
* Mail Management"""

[[experience]]
  title = "Gardener"
  company = "Frankton Kindergarten"
  company_url = ""
  location = "Hamilton, New Zealand"
  date_start = "2013-10-01"
  date_end = "2014-10-30"
  description = """Responsibilities: 
* Weed maintenance     
* Dispose of trash or waste materials      
* Trim trees or other vegetation. """


+++
