+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "Wintec"
  organization_url = ""
  title = "PRINCE2® Foundation Certificate in Project Management"
  url = ""
  certificate_url = ""
  date_start = "2018-10-01"
  date_end = ""
  description = ""

[[item]]
  organization = "Wintec"
  organization_url = ""
  title = "CCNA Routing and Switching: Routing and Switching Essential"
  url = ""
  certificate_url = ""
  date_start = "2016-11-01"
  date_end = ""
  description = ""
  
[[item]]
  organization = "Wintec"
  organization_url = ""
  title = "CCNA Routing and Switching: Introduction to Networks"
  url = ""
  certificate_url = ""
  date_start = "2016-06-01"
  date_end = ""
  description = ""

+++
