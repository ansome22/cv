+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons

[[feature]]
  icon = "window-maximize"
  icon_pack = "fas"
  name = "Web Development"
  description = "50%"
  
[[feature]]
  icon = "gamepad"
  icon_pack = "fas"
  name = "Game Development"
  description = "10%"  

[[feature]]
  icon = "project-diagram"
  icon_pack = "fas"
  name = "Project Management"
  description = "10%"

[[feature]]
  icon = "object-ungroup"
  icon_pack = "fas"
  name = "Software Development"
  description = "20%"

[[feature]]
  icon = "mobile"
  icon_pack = "fas"
  name = "Mobile Development"
  description = "10%"

[[feature]]
  icon = "network-wired"
  icon_pack = "fas"
  name = "Networking Development"
  description = "10%"

# Uncomment to use emoji icons.
# [[feature]]
#  icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# [[feature]]
#  icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

+++
