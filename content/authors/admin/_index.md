---
# Display name
title: Alex Thomas

# Username (this should match the folder name)
authors:
  - admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Student

# Organizations/Affiliations
organizations:
  - name: Wintec
    url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
  - Software Development
  - Mobile Development
  - Networking Development
  - Linux Server Development
  - Web Development
  - Game Development
  - Database Development

education:
  courses:
    - course: Master of ICT
      institution: Wintec
      year: 2020
    - course: Bachelor of ICT
      institution: Wintec
      year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: "mailto:alethoa6@gmail.com"
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/ansome22
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Researchers
  - Visitors
---

As far as what careers I want to do is a lot longer than what i do not want to do. As far as I have learnt is I do not like project management and networking. Most due to their not being a challenge or not something new to learn. Security will always have a new vulnerability. Software and websites will always evolve and a desire for new features to develop. Business Intelligence will always strive for a better understanding on their business and their customers.
